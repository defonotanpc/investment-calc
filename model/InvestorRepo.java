package com.example.demo.models;

import org.springframework.data.repository.CrudRepository;

public interface InvestorRepo extends CrudRepository<Investor, Long> {
}

