package com.example.demo.models;

import org.springframework.data.jpa.repository.JpaRepository;

public interface InvestmentRepo extends JpaRepository<Investment, Long> {
}

