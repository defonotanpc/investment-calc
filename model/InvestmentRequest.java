package com.example.demo.models;

import java.math.BigDecimal;

public class InvestmentRequest {
    private String investorName;
    private Float investmentAmount;
    private int years;
    private double returnRate;


    public String getInvestorName() {
        return investorName;
    }

    public void setInvestorName(String investorName) {
        this.investorName = investorName;
    }

    public Float getInvestmentAmount() {
        return investmentAmount;
    }

    public void setInvestmentAmount(Float investmentAmount) {
        this.investmentAmount = investmentAmount;
    }

    public int getYears() {
        return years;
    }

    public void setYears(int years) {
        this.years = years;
    }

    public double getReturnRate() {
        return returnRate;
    }

    public void setReturnRate(double returnRate) {
        this.returnRate = returnRate;
    }

}

