package com.example.demo.models;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class InvestmentService {
    @Autowired
    private InvestorRepo investorRepository;

    @Autowired
    private InvestmentRepo investmentRepository;

    public Investor saveInvestor(Investor investor) {
        return investorRepository.save(investor);
    }

    public void saveInvestment(Investment investment) {
        investmentRepository.save(investment);
    }
}

