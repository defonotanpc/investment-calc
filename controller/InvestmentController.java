package com.example.demo.controllers;

import com.example.demo.dto.InvestmentDTO;
import com.example.demo.models.Investment;
import com.example.demo.models.InvestmentRequest;
import com.example.demo.models.InvestmentService;
import com.example.demo.models.Investor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

@RestController
@RequestMapping("/investments")
public class InvestmentController {
    @Autowired
    private InvestmentService investmentService;

    @PostMapping("/calculate")
    public List<InvestmentDTO> calculateInvestments(@RequestBody InvestmentRequest request) {
        List<InvestmentDTO> result = new ArrayList<>();
        Investor investor = new Investor();
        investor.setName(request.getInvestorName());
        investor = investmentService.saveInvestor(investor);

        BigDecimal investmentAmount = BigDecimal.valueOf(request.getInvestmentAmount());
        float returnRate = (float) request.getReturnRate();
        int years = request.getYears();

        for (int year = 0; year < years; year++) {
            BigDecimal profit = investmentAmount.multiply(BigDecimal.valueOf(returnRate / 100));
            Investment investment = new Investment();
            investment.setInvestor(investor);
            investment.setYear(2023 + year);
            investment.setInvestmentAmount(investmentAmount);
            investment.setReturnRate(returnRate);
            investment.setProfit(profit);
            investmentService.saveInvestment(investment);

            InvestmentDTO investmentDTO = new InvestmentDTO();
            investmentDTO.setYear(2023 + year);
            investmentDTO.setInvestmentAmount(investmentAmount);
            investmentDTO.setReturnRate(returnRate);
            investmentDTO.setProfit(profit);
            result.add(investmentDTO);

            investmentAmount = investmentAmount.add(profit);
        }

        return result;
    }
}
